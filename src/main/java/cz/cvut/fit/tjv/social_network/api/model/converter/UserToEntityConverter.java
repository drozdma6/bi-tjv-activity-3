package cz.cvut.fit.tjv.social_network.api.model.converter;

import cz.cvut.fit.tjv.social_network.api.model.UserDto;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserToEntityConverter implements Function<UserDto, User> {
    @Override
    public User apply(UserDto userDto) {
        return new User(userDto.getUsername(), userDto.getRealName());
    }
}
