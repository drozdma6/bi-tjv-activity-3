/*
 * Project Social Network. Created for Java Technology course at Czech Technical University in Prague,
 * Faculty of Information Technology.
 *
 * Author: Ondřej Guth (ondrej.guth@fit.cvut.cz)
 *
 * This code is intended for educational purposes only.
 */

package cz.cvut.fit.tjv.social_network.business;

import cz.cvut.fit.tjv.social_network.dao.PostRepository;
import cz.cvut.fit.tjv.social_network.domain.Post;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Business logic operations related to any Post.
 */
@Service
public class PostService extends AbstractCrudService<Post, Long> {
    protected UserService userService;

    public PostService(PostRepository postRepository, UserService userService) {
        super(postRepository); //invocation of constructor of the superclass (AbstractCrudService)
        this.userService = userService;
    }

    /**
     * Add or remove like of specified user to specified post.
     *
     * @param id       key of the post
     * @param username key of the user
     * @throws NoSuchElementException if the specified post or user do not exist
     */
    @Transactional
    public void updateLikesInPost(long id, String username) {
        //an example of complex business operation
        Optional<Post> optionalPost = readById(id);
        Optional<User> optionalUser = userService.readById(username);
        Post post = optionalPost.orElseThrow(); //throws NoSuchElementException if the Optional is empty
        User user = optionalUser.orElseThrow();
        if(post.getLiked().contains(user)){
            post.removeLike(user);
        }else{
            post.addLike(user);
        }
    }

    /**
     * Read Posts liked by more than specified number of users.
     * @param likes number of users
     * @return a Collection of Posts that are liked by more than likes number of users
     */
    public Collection<Post> findLikedMoreThan(int likes) {
        return ((PostRepository) repository).findByLikedGreaterThan(likes);
    }

    /**
     * Read posts created by specified user.
     *
     * @param authorId username of the posts' author
     * @return a collection of posts created by specified user; it is never null
     */
    public Collection<Post> readAllByAuthor(String authorId) {
        return ((PostRepository) repository).findAllByAuthorUsername(authorId); //typecast from AbstractFileRepository to PostFileRepository
    }

    public Collection<User> getAllUsersWhoLikedPost(Long postId){
        return repository.findById(postId).orElseThrow(
                NoSuchElementException::new
        ).getLiked();
    }
}
