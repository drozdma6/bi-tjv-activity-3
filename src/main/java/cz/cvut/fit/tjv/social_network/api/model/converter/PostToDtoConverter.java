package cz.cvut.fit.tjv.social_network.api.model.converter;

import cz.cvut.fit.tjv.social_network.api.model.PostDto;
import cz.cvut.fit.tjv.social_network.domain.Post;
import org.springframework.stereotype.Component;

import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class PostToDtoConverter implements Function<Post, PostDto> {
    @Override
    public PostDto apply(Post post) {
        var dto = new PostDto();
        UserToDtoConverter userToDtoConverter = new UserToDtoConverter();
        dto.setId(post.getId());
        dto.setAuthorUsername(post.getAuthor().getUsername());
        dto.setLiked(post.getLiked().stream().map(userToDtoConverter).collect(Collectors.toSet()));
        return dto;
    }
}
