/*
 * Copyright (c) 2022. Czech Technical University in Prague, Faculty of Information Technology.
 *
 * Project Social Network. Created for Java Technology course.
 *
 *  Authors:
 *  Ondřej Guth (ondrej.guth@fit.cvut.cz)
 *  Jan Blizničenko (jan.bliznicenko@fit.cvut.cz)
 *  Filip Glazar (glazafil@fit.cvut.cz)
 *
 *  This code is intended for educational purposes only.
 *
 */

package cz.cvut.fit.tjv.social_network.dao.in_memory;

import cz.cvut.fit.tjv.social_network.dao.CrudRepository;
import cz.cvut.fit.tjv.social_network.domain.DomainEntity;
import cz.cvut.fit.tjv.social_network.domain.User;

import java.util.*;

/**
 * Data layer implemented as in-memory-only storage, supports CRUD.
 * @param <T> entity type
 * @param <ID> primary key type
 */
public class InMemoryRepository<T extends DomainEntity<ID>, ID> implements CrudRepository<T, ID> {
    private Map<ID, T> database = new HashMap<>();
    @Override
    public T save(T e) {
        database.put(e.getId(), e);
        return e;
    }

    @Override
    public boolean existsById(ID id) {
        return database.containsKey(id);
    }

    @Override
    public Optional<T> findById(ID id) {
        return Optional.ofNullable(database.get(id));
    }

    @Override
    public Collection<T> findAll() {
        return database.values();
    }

    @Override
    public void deleteById(ID id) {
        database.remove(id);
    }
}
