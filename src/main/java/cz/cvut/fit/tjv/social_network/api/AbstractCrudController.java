package cz.cvut.fit.tjv.social_network.api;

import cz.cvut.fit.tjv.social_network.business.AbstractCrudService;
import cz.cvut.fit.tjv.social_network.business.EntityStateException;
import cz.cvut.fit.tjv.social_network.domain.DomainEntity;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collection;
import java.util.function.Function;

public class AbstractCrudController<E extends DomainEntity<ID>, D, ID> {
    protected AbstractCrudService<E, ID> service;
    protected Function<E, D> toDtoConverter;
    protected Function<D, E> toEntityConverter;

    public AbstractCrudController(AbstractCrudService<E, ID> service, Function<E, D> toDtoConverter, Function<D, E> toEntityConverter) {
        this.service = service;
        this.toDtoConverter = toDtoConverter;
        this.toEntityConverter = toEntityConverter;
    }
    
    @PostMapping
    public D create (@RequestBody D e) {
        E entity = toEntityConverter.apply(e);
        try{
            service.create(entity);
        }catch(EntityStateException exception){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        entity = service.readById(entity.getId()).orElseThrow(
                () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
        );
        return toDtoConverter.apply(entity);
    }

    @GetMapping
    public Collection<D> readAll() {
        return service.readAll().stream().map(toDtoConverter).toList();
    }

    @PutMapping("/{id}")
    public D update(@RequestBody D e, @PathVariable("id") ID id) {
        service.readById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND)
        );
        E entity = toEntityConverter.apply(e);
        try{
            service.update(entity);
        }catch(EntityStateException exception){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        entity = service.readById(entity.getId()).orElseThrow(
                () -> new ResponseStatusException(
                        HttpStatus.NOT_FOUND, "User Not Found")
        );
        return toDtoConverter.apply(entity);
    }

//    DELETE /users/{id}
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable ("id") ID id) {
        service.deleteById(id);
    }
}
