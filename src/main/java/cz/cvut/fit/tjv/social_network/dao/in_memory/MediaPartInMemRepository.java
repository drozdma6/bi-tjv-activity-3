package cz.cvut.fit.tjv.social_network.dao.in_memory;

import cz.cvut.fit.tjv.social_network.dao.MediaPartRepository;
import cz.cvut.fit.tjv.social_network.domain.MediaPart;
import cz.cvut.fit.tjv.social_network.domain.MediaPartKey;
import org.springframework.stereotype.Component;

@Component
public class MediaPartInMemRepository extends InMemoryRepository<MediaPart, MediaPartKey> implements MediaPartRepository {
}
