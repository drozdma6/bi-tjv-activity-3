package cz.cvut.fit.tjv.social_network.api;

import cz.cvut.fit.tjv.social_network.api.model.UserDto;
import cz.cvut.fit.tjv.social_network.api.model.converter.UserToDtoConverter;
import cz.cvut.fit.tjv.social_network.api.model.converter.UserToEntityConverter;
import cz.cvut.fit.tjv.social_network.business.AbstractCrudService;
import cz.cvut.fit.tjv.social_network.business.UserService;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController extends AbstractCrudController<User, UserDto, String> {
    public UserController(UserService service, UserToDtoConverter toDtoConverter, UserToEntityConverter toEntityConverter) {
        super(service, toDtoConverter, toEntityConverter);
    }
}
