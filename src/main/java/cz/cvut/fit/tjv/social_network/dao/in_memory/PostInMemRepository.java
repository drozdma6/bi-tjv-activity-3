package cz.cvut.fit.tjv.social_network.dao.in_memory;

import cz.cvut.fit.tjv.social_network.dao.PostRepository;
import cz.cvut.fit.tjv.social_network.domain.Post;
import org.springframework.stereotype.Component;

import java.util.Collection;

//@Component
public class PostInMemRepository extends InMemoryRepository<Post, Long> implements PostRepository {
    public Collection<Post> findByLikedGreaterThan(int likes) {
        return findAll().stream().filter(p -> p.getLiked().size() > likes).toList();
    }

    public Collection<Post> findAllByAuthorUsername(String userId) {
        return findAll().stream().filter(p -> p.getAuthor().getUsername().equals(userId)).toList();
    }
}
