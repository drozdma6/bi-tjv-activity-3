package cz.cvut.fit.tjv.social_network.api.model.converter;

import cz.cvut.fit.tjv.social_network.api.model.UserDto;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class UserToDtoConverter implements Function<User, UserDto> {
    @Override
    public UserDto apply(User user) {
        var ret = new UserDto();
        ret.setUsername(user.getUsername());
        ret.setRealName(user.getRealName());
        return ret;
    }
}
