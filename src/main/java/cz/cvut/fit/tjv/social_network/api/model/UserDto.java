package cz.cvut.fit.tjv.social_network.api.model;

//JsonView
public class UserDto {
    private String username;
    private String realName;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
