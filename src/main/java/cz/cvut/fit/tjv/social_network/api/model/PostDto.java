package cz.cvut.fit.tjv.social_network.api.model;

import java.util.Set;

public class PostDto {
    private Long id;
    private String textContents;
    private String authorUsername;
    private Set<UserDto> liked;

    public void setLiked(Set<UserDto> liked){
        this.liked = liked;
    }

    public Set<UserDto> getLiked(){
        return liked;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTextContents() {
        return textContents;
    }

    public void setTextContents(String textContents) {
        this.textContents = textContents;
    }

    public String getAuthorUsername() {
        return authorUsername;
    }

    public void setAuthorUsername(String authorUsername) {
        this.authorUsername = authorUsername;
    }
}
