package cz.cvut.fit.tjv.social_network.dao.in_memory;

import cz.cvut.fit.tjv.social_network.dao.UserRepository;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.stereotype.Component;

//@Component
public class UserInMemRepository extends InMemoryRepository<User, String> implements UserRepository {
}
