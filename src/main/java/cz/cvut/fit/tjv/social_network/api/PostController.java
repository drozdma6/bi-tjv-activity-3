package cz.cvut.fit.tjv.social_network.api;

import cz.cvut.fit.tjv.social_network.api.model.PostDto;
import cz.cvut.fit.tjv.social_network.api.model.UserDto;
import cz.cvut.fit.tjv.social_network.api.model.converter.PostToDtoConverter;
import cz.cvut.fit.tjv.social_network.api.model.converter.UserToDtoConverter;
import cz.cvut.fit.tjv.social_network.api.model.converter.UserToEntityConverter;
import cz.cvut.fit.tjv.social_network.business.EntityStateException;
import cz.cvut.fit.tjv.social_network.business.PostService;
import cz.cvut.fit.tjv.social_network.business.UserService;
import cz.cvut.fit.tjv.social_network.domain.Post;
import cz.cvut.fit.tjv.social_network.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/posts")
public class PostController extends AbstractCrudController<Post, PostDto, Long>{
    public PostController(PostService service, UserService userService){
        super(service, new PostToDtoConverter(), d -> {
                  Post post = new Post(d.getId(), null, userService.readById(d.getAuthorUsername()).get(),
                                       null);
                  post.addLike(d.getLiked().stream().map(new UserToEntityConverter())
                                       .collect(Collectors.toSet()));
                  return post;
              }
        );
    }

    @PutMapping("/{idPost}/users/{idUser}")
    public PostDto updateLike(@PathVariable Long idPost, @PathVariable String idUser){
        try{
            ((PostService) service).updateLikesInPost(idPost, idUser);
        } catch (NoSuchElementException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return toDtoConverter.apply(service.readById(idPost).get());
    }


    @GetMapping("{id}/users")
    public Collection<UserDto> getAllUsersWhoLikedPost(@PathVariable Long id){
        Function<? super User, ?> UserToDtoConverter = new UserToDtoConverter();
        List<UserDto> users = new ArrayList<>();
        for (User user : ((PostService) service).getAllUsersWhoLikedPost(id)){
            users.add((UserDto) UserToDtoConverter.apply(user));
        }
        return users;
    }

    @GetMapping("/users/{id}")
    public Collection<PostDto> getAllPostsCreatedByUser(@PathVariable String id){
        return ((PostService) service).readAllByAuthor(id).stream().map(toDtoConverter).toList();
    }
}
